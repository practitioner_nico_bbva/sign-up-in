import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles, } from '@cells-components/cells-lit-helpers/cells-lit-helpers.js';
import {classMap} from 'lit-html/directives/class-map.js';

import '@bbva-web-components/bbva-input-field';
import '@bbva-web-components/bbva-button-default';

import styles from './sign-up-in-styles.js';

/**
This component visual, utilizado para mostar login y signup.

Example:

```html
<sign-up-in></sign-up-in>
```
* @customElement
* @demo demo/index.html
* @extends {LitElement}
*/
export class SignUpIn extends LitElement {
  static get is() {
    return 'sign-up-in';
  }

  // Declare properties
  static get properties() {
    return {
      newUser: Boolean,
      mail: String,
      user: String,
      password: String
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.newUser = false;
  }

  static get shadyStyles() {
    return `
      ${styles.cssText}
      ${getComponentSharedStyles('sign-up-in-shared-styles').cssText}
    `;
  }

  /**
   * Envia el usuario con la contraseña y el mail, para que lo procese el DM.
   * Puede ser un usuario nuevo, o login.
   */
  _sendUser() {
    const { user, password, mail } = this;
    if(this.newUser) {
      return this.dispatchEvent(new CustomEvent('sign_up_user', { detail: { user, password, mail }, composed:true, bubbles: true }));
    }
    return this.dispatchEvent(new CustomEvent('sign_in_user', { detail: { mail, password }, composed: true, bubbles: true }));
  }

  /**
   * Asigan el valor a la prop correspondiente.
   * @param {Object} e Detalle del evento
   */
  _setInputValue(e) {
    this[e.target.name] = e.target.value;
  }

  // Define a template
  render() {
    return html`
      <style>${this.constructor.shadyStyles}</style>
      <slot></slot>
      <h1> Inicie sesión para almacenar sus recordatorios.</h1>
      <section class="${classMap({ 'sign-up-in-container': true, 'is-new-user': this.newUser } )}">
        <h1>${ this.mainTitle }</h1>  
        <bbva-input-field name="mail" type="email" label="Ingrese mail" max-length="25" @input="${ this._setInputValue }"></bbva-input-field>

        <bbva-input-field name="password" label="Ingrese contraseña" accessibillity-text-input-icon="delete input content" type="password" @input="${ this._setInputValue }"></bbva-input-field>
        
        <bbva-input-field name="user" class="input-name" label="Ingrese Usuario" type="text" max-length="25" @input="${ this._setInputValue }"></bbva-input-field>
        
        <bbva-button-default @click="${ this._sendUser }">${ this.newUser ? 'Crear Usuario' : 'Iniciar sesión' }</bbva-button-default>

        <a class="create-user" @click="${ () => this.newUser = !this.newUser }"> ${ this.newUser ? 'Volver' : 'Crear usuario' } </a>
      </section>
    `;
  }
}

// Register the element with the browser
customElements.define(SignUpIn.is, SignUpIn);
