import { css, } from 'lit-element';


export default css`:host {
  display: block;
  box-sizing: border-box;
  @apply --sign-up-in; }

:host([hidden]), [hidden] {
  display: none !important; }

*, *:before, *:after {
  box-sizing: inherit; }

.sign-up-in-container {
  padding: 20px; }
  .sign-up-in-container .input-name {
    max-height: 200px;
    transition: all .5s ease-in-out; }
  .sign-up-in-container:not(.is-new-user) .input-name {
    margin: 0;
    height: auto;
    max-height: 0;
    overflow: hidden;
    visibility: hidden; }
  .sign-up-in-container bbva-button-default {
    width: 100%; }
  .sign-up-in-container .create-user {
    margin-top: 25px;
    display: block;
    text-align: center;
    color: #212121;
    font-size: 14px; }

h1 {
  font-weight: 400;
  text-align: center;
  font-size: 18px;
  margin: 0;
  margin-top: 10%; }

bbva-input-field {
  margin-bottom: 20px; }
`;